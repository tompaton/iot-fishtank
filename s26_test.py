import sys
from unittest.mock import Mock, call

import pytest


@pytest.fixture
def s26():
    for module in ['machine', 'hardware']:
        sys.modules[module] = Mock()

    import s26 as s26_module

    return s26_module


def test_s26(s26):
    assert s26.Pin.mock_calls == [call(12, s26.Pin.OUT)]
    assert s26.Led.mock_calls == [call(pin=13)]
    assert s26.DebouncedButton.mock_calls == [call(pin=0)]
