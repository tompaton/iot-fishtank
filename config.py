wifi_name = ''
wifi_password = ''
mqtt_server = ''
mqtt_uid = ''
mqtt_pwd = ''
mqtt_topic = 'places/taylor-st/fishtank'
mqtt_keepalive = 60

feeding_duration = 30 * 60
fed_duration = 12 * 60 * 60

heartbeat_interval = 30
