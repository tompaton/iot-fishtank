import config
import relay
import s26
from hardware import Mqtt, Scheduler, Wifi

import comms

scheduler = Scheduler()

wifi = Wifi(config.wifi_name, config.wifi_password)

mqtt = Mqtt(
    config.mqtt_server,
    config.mqtt_uid,
    config.mqtt_pwd,
    config.mqtt_topic,
    config.mqtt_keepalive,
)

c = comms.Comms(scheduler.delay, mqtt, wifi, mqtt.publish, config.heartbeat_interval)

r = relay.Relay(
    s26.RELAY,
    s26.LED,
    scheduler.delay,
    mqtt.publish,
    config.feeding_duration,
    config.fed_duration,
)


def on_button(arg):
    r.on_event(arg)
    mqtt.publish("1", topic="/button")


s26.BUTTON.on_button(on_button, "button")
mqtt.on_message(on_button, "button")


def on_tick():
    return mqtt.check(resend_queued=False)


scheduler.loop(on_tick=on_tick)
