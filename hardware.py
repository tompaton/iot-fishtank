import time

import micropython
import network
import ubinascii
from machine import Pin, Timer, unique_id

from umqtt_simple import MQTTClient


def client_id():
    return ubinascii.hexlify(unique_id()).decode("ascii")


class Scheduler:
    def __init__(self):
        self._once = {}

    def delay(self, interval, function, name, arg=None):
        if interval is None:
            print("scheduler: clear {}".format(name))
            self._once.pop(id(function), None)
        else:
            print("scheduler: delay {} {} {}".format(name, interval, arg))
            self._once[id(function)] = (time.time() + interval, function, name, arg)

    def tick(self):
        done = []

        for start_time, function, name, arg in self._once.values():
            if time.time() >= start_time:
                print("scheduler: run {} {}".format(name, arg))
                function(arg)
                done.append(id(function))

        for function_id in done:
            self._once.pop(function_id, None)

    def loop(self, on_tick=None):  # pragma: nocover
        while True:
            self.tick()
            if callable(on_tick):
                on_tick()
            time.sleep(1)


class Led:
    def __init__(self, pin=0):
        self.pin = Pin(pin, Pin.OUT)  # Note: inverted

    def on(self):
        self.pin.off()

    def off(self):
        self.pin.on()


class DebouncedButton:
    def __init__(self, pin=0, period=100):
        self.pin = Pin(int(pin), Pin.IN, Pin.PULL_UP)
        self.timer = Timer(-1)

        self.pin.irq(handler=self.irq_cb)
        self.timer.init(period=period, callback=self.tick_cb)

        self.count = self.ready1 = self.ready2 = 0

        self._handle = self.handle
        self._arg = None

    def on_button(self, handler, arg):
        self._handle = handler
        self._arg = arg

    def irq_cb(self, pin):
        self.count += 1

    def tick_cb(self, t):
        # ignore until button is off
        if not self.pin.value():
            return

        if self.ready1 != self.count:
            # got a trigger, need to wait until it's stable
            self.ready1 = self.count

            # self.count (and self.ready1) could have overflowed and be less
            # than self.ready2 now.

            # assert self.ready1 != self.ready2

        elif self.ready2 != self.ready1:
            # has been stable for 2 ticks

            # if we were interrupted between if and elif, then self.ready1 !=
            # self.count any more, but that's ok as we'll count another pulse
            # next time around.

            # assert self.ready1 <= self.count (unless self.count has overflowed
            # during interrupt)

            self.ready2 = self.ready1

            micropython.schedule(self._handle, self._arg)

        else:
            # assert self.ready2 == self.ready1 <= self.count (unless self.count
            # has overflowed during interrupt)
            pass

    def handle(self, arg):  # pragma: nocover
        pass


class Wifi:
    def __init__(self, wifi_name=None, wifi_password=None):
        self.wifi_name = wifi_name
        self.wifi_password = wifi_password

        # disable access point
        network.WLAN(network.AP_IF).active(False)

        self.wifi = network.WLAN(network.STA_IF)

    def isconnected(self):
        if self.wifi.isconnected():
            print("wifi: connected")
            return True
        else:
            print("wifi: not connected")
            return False

    def connect(self):
        if self.isconnected():
            return True

        print("wifi: connect...")
        self.wifi.active(True)
        self.wifi.connect(self.wifi_name, self.wifi_password)

        return self.isconnected()


class Mqtt:
    def __init__(self, mqtt_server, mqtt_uid, mqtt_pwd, mqtt_topic, mqtt_keepalive):
        self.client_id = client_id()
        self.mqtt = MQTTClient(
            self.client_id,
            mqtt_server,
            keepalive=mqtt_keepalive,
            user=mqtt_uid,
            password=mqtt_pwd,
        )
        self.mqtt.set_last_will(**self._status("OFFLINE"))
        self.mqtt.set_callback(self.message_callback)
        self.mqtt_topic = mqtt_topic

        self._msg = None

        self._handle = self.handle
        self._arg = None

    def on_message(self, handler, arg):
        self._handle = handler
        self._arg = arg

    def handle(self, arg):
        pass

    def _status(self, msg):
        return {
            "topic": "status/" + self.client_id,
            "msg": msg,
            "retain": True,
            "qos": 1,
        }

    def message_callback(self, topic, msg_raw):
        topic = topic.decode("ascii")
        print('mqtt: callback "{}"'.format(topic))
        if topic == self.mqtt_topic + "/feeding":
            print("mqtt: feeding")
            micropython.schedule(self._handle, self._arg)

    def connect(self):
        try:
            print("mqtt: connect...")
            self.mqtt.connect()

            self.mqtt.subscribe(self.mqtt_topic + "/feeding")

            self.mqtt.publish(**self._status("ONLINE 1.0.0"))

        except Exception as err:
            print("mqtt: not connected")
            self.disconnect()
            return False

        else:
            print("mqtt: connected")
            return True

    def check(self, resend_queued=True):
        try:
            if self._msg and resend_queued:
                print("mqtt: sending queued msg")
                if self.mqtt.publish(self.mqtt_topic, self._msg):
                    self._msg = None

            self.mqtt.check_msg()
            self.mqtt.ping()

        except Exception as err:
            print("mqtt: error {}".format(err))
            self.disconnect()
            return False

        else:
            print("mqtt: ok")
            return True

    def publish(self, msg, topic=""):
        print('mqtt: publish "{}"'.format(msg))

        try:
            self.mqtt.publish(self.mqtt_topic + topic, msg)
            return True
        except:
            print("mqtt: queued msg")
            self._msg = msg
            return False

    def disconnect(self):
        try:
            self.mqtt.disconnect()
        except:
            pass
