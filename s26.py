from machine import Pin

from hardware import DebouncedButton, Led

RELAY = Pin(12, Pin.OUT)

LED = Led(pin=13)

BUTTON = DebouncedButton(pin=0)
