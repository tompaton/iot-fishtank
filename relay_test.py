from unittest.mock import Mock, call

import relay


def test_start():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer')]
    assert relay2.mock_calls == [call.on()]
    assert led.mock_calls == [call.on()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed')]


def test_fed_timer():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('timer')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(None, r.on_event, 'relay')]
    assert relay2.mock_calls == [call.on(), call.on()]
    assert led.mock_calls == [call.on(), call.off()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"due"', topic='/status'),
                                 call('fed:timer -> due')]


def test_fed_button():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('button')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(30, r.on_event, 'relay', 'timer')]
    assert relay2.mock_calls == [call.on(), call.off()]
    assert led.mock_calls == [call.on(), call.on()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"feeding"', topic='/status'),
                                 call('fed:button -> feeding')]


def test_due_timer():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('timer')
    r.on_event('timer')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(None, r.on_event, 'relay')]
    assert relay2.mock_calls == [call.on(), call.on()]
    assert led.mock_calls == [call.on(), call.off()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"due"', topic='/status'),
                                 call('fed:timer -> due'),
                                 call('due:timer -> due')]


def test_due_button():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('timer')
    r.on_event('button')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(None, r.on_event, 'relay'),
                                call(30, r.on_event, 'relay', 'timer')]
    assert relay2.mock_calls == [call.on(), call.on(), call.off()]
    assert led.mock_calls == [call.on(), call.off(), call.on()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"due"', topic='/status'),
                                 call('fed:timer -> due'),
                                 call('"feeding"', topic='/status'),
                                 call('due:button -> feeding')]


def test_feeding_button():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('button')
    r.on_event('button')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(30, r.on_event, 'relay', 'timer')]
    assert relay2.mock_calls == [call.on(), call.off()]
    assert led.mock_calls == [call.on(), call.on()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"feeding"', topic='/status'),
                                 call('fed:button -> feeding'),
                                 call('feeding:button -> feeding')]


def test_feeding_timer():
    relay2 = Mock()
    led = Mock()
    timer = Mock()
    notify = Mock()
    r = relay.Relay(relay2, led, timer, notify,
                    feeding_duration=30, fed_duration=12,
                    msg_format="{old}:{event} -> {new}")
    assert r.state == 'fed'
    r.on_event('button')
    r.on_event('timer')
    assert timer.mock_calls == [call(12, r.on_event, 'relay', 'timer'),
                                call(30, r.on_event, 'relay', 'timer'),
                                call(12, r.on_event, 'relay', 'timer')]
    assert relay2.mock_calls == [call.on(), call.off(), call.on()]
    assert led.mock_calls == [call.on(), call.on(), call.on()]
    assert notify.mock_calls == [call('"fed"', topic='/status'),
                                 call('start:start -> fed'),
                                 call('"feeding"', topic='/status'),
                                 call('fed:button -> feeding'),
                                 call('"fed"', topic='/status'),
                                 call('feeding:timer -> fed')]
