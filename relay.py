from state import State


class Relay(State):

    transition = {
        'start': {
            'start': 'fed',
        },
        'feeding': {
            'timer': 'fed',
        },
        'fed': {
            'button': 'feeding',
            'timer': 'due',
        },
        'due': {
            'button': 'feeding',
        },
    }

    def __init__(self, relay=None, led=None, timer=None, notify=None,
                 feeding_duration=30*60, fed_duration=12*60*60,
                 msg_format=None):
        self.relay = relay
        self.led = led
        self.timer = timer
        self.feeding_duration = feeding_duration
        self.fed_duration = fed_duration

        super().__init__(notify, msg_format)

    def feeding(self):
        self.relay.off()
        self.led.on()
        self.timer(self.feeding_duration, self.on_event, 'relay', 'timer')
        self.notify('"feeding"', topic='/status')

    def fed(self):
        self.relay.on()
        self.led.on()
        self.timer(self.fed_duration, self.on_event, 'relay', 'timer')
        self.notify('"fed"', topic='/status')

    def due(self):
        self.relay.on()
        self.led.off()
        self.timer(None, self.on_event, 'relay')
        self.notify('"due"', topic='/status')
